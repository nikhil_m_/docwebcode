import { Component, OnInit } from '@angular/core';
import { DatabaseService } from '../database.service';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
	constructor(public db: DatabaseService) { }

	ngOnInit() {

		this.db.fetchData('get_header_data').subscribe(r => {
			console.log(r);
			this.db.last_updated = r["time"]["updated_time"];
			this.db.cad_rate = r["cad_rate"]["value"];
		},error=>{
			console.log(error);  
		});

	}

}
