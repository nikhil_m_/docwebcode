import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class DatabaseService {
	url:string = 'http://localhost/docweb/api/';
	public last_updated = '';
	public cad_rate = '';
	constructor( public http: HttpClient) { }

	header: any = new HttpHeaders();

	fetchData(fn:any){
		this.header.append('Content-Type', 'application/json');
		return this.http.get( this.url+fn, {headers: this.header})
	}

}
