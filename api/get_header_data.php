<?php
header('Access-Control-Allow-Origin: *');
include 'connection.php';


$select = mysqli_query($link,"SELECT * FROM info");
$row = mysqli_fetch_array($select,MYSQLI_ASSOC);

$row["updated_time"] = date('m/d/Y H:i:s', $row["timestampval"]);

$rate = mysqli_query($link,"SELECT * FROM rates where currency = 'CAD'  ");
$rate = mysqli_fetch_array($rate,MYSQLI_ASSOC);

$data = array('time' => $row , 'cad_rate' => $rate );

echo json_encode($data);