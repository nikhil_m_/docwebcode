-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 14, 2018 at 03:51 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `docweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

DROP TABLE IF EXISTS `info`;
CREATE TABLE IF NOT EXISTS `info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `base` varchar(10) NOT NULL,
  `timestampval` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`id`, `base`, `timestampval`) VALUES
(1, 'USD', 1539529203);

-- --------------------------------------------------------

--
-- Table structure for table `rates`
--

DROP TABLE IF EXISTS `rates`;
CREATE TABLE IF NOT EXISTS `rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency` varchar(10) NOT NULL,
  `value` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=172 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rates`
--

INSERT INTO `rates` (`id`, `currency`, `value`) VALUES
(1, 'AED', 3.673014),
(2, 'AFN', 75.965048),
(3, 'ALL', 108.45),
(4, 'AMD', 485.608915),
(5, 'ANG', 1.775007),
(6, 'AOA', 301.425),
(7, 'ARS', 36.644),
(8, 'AUD', 1.405679),
(9, 'AWG', 1.799996),
(10, 'AZN', 1.7025),
(11, 'BAM', 1.5941),
(12, 'BBD', 2),
(13, 'BDT', 83.82),
(14, 'BGN', 1.68888),
(15, 'BHD', 0.377006),
(16, 'BIF', 1810.5),
(17, 'BMD', 1),
(18, 'BND', 1.410206),
(19, 'BOB', 6.911803),
(20, 'BRL', 3.783),
(21, 'BSD', 1),
(22, 'BTC', 0.000160200588),
(23, 'BTN', 73.993696),
(24, 'BWP', 10.642022),
(25, 'BYN', 2.126364),
(26, 'BZD', 2.009236),
(27, 'CAD', 1.304053),
(28, 'CDF', 1626),
(29, 'CHF', 0.99194),
(30, 'CLF', 0.025048),
(31, 'CLP', 679.7),
(32, 'CNH', 6.922582),
(33, 'CNY', 6.9222),
(34, 'COP', 3090.933438),
(35, 'CRC', 593.983357),
(36, 'CUC', 1),
(37, 'CUP', 25.75),
(38, 'CVE', 95.7235),
(39, 'CZK', 22.3034),
(40, 'DJF', 178.05),
(41, 'DKK', 6.4543),
(42, 'DOP', 50),
(43, 'DZD', 118.33711),
(44, 'EGP', 17.906),
(45, 'ERN', 14.998065),
(46, 'ETB', 27.955),
(47, 'EUR', 0.863895),
(48, 'FJD', 2.160005),
(49, 'FKP', 0.760225),
(50, 'GBP', 0.760225),
(51, 'GEL', 2.44699),
(52, 'GGP', 0.760225),
(53, 'GHS', 4.7937),
(54, 'GIP', 0.760225),
(55, 'GMD', 49.45),
(56, 'GNF', 9115),
(57, 'GTQ', 7.736978),
(58, 'GYD', 208.911761),
(59, 'HKD', 7.83545),
(60, 'HNL', 24.069972),
(61, 'HRK', 6.413012),
(62, 'HTG', 69.709862),
(63, 'HUF', 280.48),
(64, 'IDR', 15197.455),
(65, 'ILS', 3.6242),
(66, 'IMP', 0.760225),
(67, 'INR', 73.686),
(68, 'IQD', 1191),
(69, 'IRR', 42497.185464),
(70, 'ISK', 116.259783),
(71, 'JEP', 0.760225),
(72, 'JMD', 134.56),
(73, 'JOD', 0.709506),
(74, 'JPY', 112.213),
(75, 'KES', 100.847382),
(76, 'KGS', 68.541782),
(77, 'KHR', 4083),
(78, 'KMF', 425.901871),
(79, 'KPW', 900),
(80, 'KRW', 1131.35),
(81, 'KWD', 0.303318),
(82, 'KYD', 0.83216),
(83, 'KZT', 370.57),
(84, 'LAK', 8523.5),
(85, 'LBP', 1514.242335),
(86, 'LKR', 169.647416),
(87, 'LRD', 156.949873),
(88, 'LSL', 14.555),
(89, 'LYD', 1.375),
(90, 'MAD', 9.44776),
(91, 'MDL', 16.944933),
(92, 'MGA', 3457.5),
(93, 'MKD', 53.308123),
(94, 'MMK', 1563.482011),
(95, 'MNT', 2484.433144),
(96, 'MOP', 8.06797),
(97, 'MRO', 357),
(98, 'MRU', 35.9),
(99, 'MUR', 34.3335),
(100, 'MVR', 15.450044),
(101, 'MWK', 720),
(102, 'MXN', 18.856018),
(103, 'MYR', 4.154464),
(104, 'MZN', 60.500809),
(105, 'NAD', 14.555),
(106, 'NGN', 363),
(107, 'NIO', 32.17),
(108, 'NOK', 8.1746),
(109, 'NPR', 118.390741),
(110, 'NZD', 1.537043),
(111, 'OMR', 0.384984),
(112, 'PAB', 1),
(113, 'PEN', 3.3345),
(114, 'PGK', 3.34525),
(115, 'PHP', 54.002971),
(116, 'PKR', 133.25),
(117, 'PLN', 3.717),
(118, 'PYG', 5932.257915),
(119, 'QAR', 3.641),
(120, 'RON', 4.035287),
(121, 'RSD', 102.31),
(122, 'RUB', 66.065),
(123, 'RWF', 871),
(124, 'SAR', 3.75095),
(125, 'SBD', 7.938124),
(126, 'SCR', 13.619981),
(127, 'SDG', 47.7),
(128, 'SEK', 8.9618),
(129, 'SGD', 1.377897),
(130, 'SHP', 0.760225),
(131, 'SLL', 8390),
(132, 'SOS', 579.5),
(133, 'SRD', 7.458),
(134, 'SSP', 130.2634),
(135, 'STD', 21083.728753),
(136, 'STN', 21.22),
(137, 'SVC', 8.746991),
(138, 'SYP', 515.013439),
(139, 'SZL', 14.555),
(140, 'THB', 32.6955),
(141, 'TJS', 9.417519),
(142, 'TMT', 3.499986),
(143, 'TND', 2.825903),
(144, 'TOP', 2.305893),
(145, 'TRY', 5.87264),
(146, 'TTD', 6.74175),
(147, 'TWD', 30.907066),
(148, 'TZS', 2287.9),
(149, 'UAH', 27.912),
(150, 'UGX', 3783.585297),
(151, 'USD', 1),
(152, 'UYU', 32.989124),
(153, 'UZS', 8195),
(154, 'VEF', 248471.708907),
(155, 'VES', 62.737514),
(156, 'VND', 23252.647672),
(157, 'VUV', 110.656271),
(158, 'WST', 2.627371),
(159, 'XAF', 566.677973),
(160, 'XAG', 0.06848144),
(161, 'XAU', 0.00082095),
(162, 'XCD', 2.70255),
(163, 'XDR', 0.714693),
(164, 'XOF', 566.677973),
(165, 'XPD', 0.0009368),
(166, 'XPF', 103.090095),
(167, 'XPT', 0.00119262),
(168, 'YER', 250.350747),
(169, 'ZAR', 14.51687),
(170, 'ZMW', 12.221),
(171, 'ZWL', 322.355011);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
