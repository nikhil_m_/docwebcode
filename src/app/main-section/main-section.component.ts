import { Component, OnInit } from '@angular/core';
import { DatabaseService } from '../database.service';

@Component({
  selector: 'app-main-section',
  templateUrl: './main-section.component.html',
  styleUrls: ['./main-section.component.css']
})
export class MainSectionComponent implements OnInit {
  data:any=[];
  p='1';
  constructor(public db: DatabaseService) { }
  
  ngOnInit() {
    this.db.fetchData('get_rates').subscribe(r => {
			console.log(r);
      this.data =r;
      
		},error=>{
			console.log(error);  
		});
  }
  
  refresh(){
    this.db.fetchData('jsontodb').subscribe(r => {
			console.log(r);
      this.db.last_updated = r["time"]["updated_time"];
      this.db.cad_rate = r["cad_rate"]["value"];

      if(r["update"] == true){
        this.db.fetchData('get_rates').subscribe(r => {
          console.log(r);
          this.data =r;
          
        },error=>{
          console.log(error);  
        });
      }
      
		},error=>{
			console.log(error);  
    });
    
    
    
  }
  
}
